/*global google*/
import React, { Component } from "react";
import axios from 'axios';
import Skeleton from 'react-loading-skeleton';
import 'react-loading-skeleton/dist/skeleton.css';
import {
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer
} from "react-google-maps";
class Map extends Component {
  state = {
    directions: null,
    partner : [],
    service : [],
    step_step_job : [],
    last_posistion : [],
    customer : [],
    curr_service : [],
    skeletonLoader:true,
    loading: true
  };

  componentDidMount(props) {
    

    axios.get(`https://olla.ws/api/landingpage/tracking?tracking_code=n5HoPv`,{
      headers: {
          'x-token-olla': 'ab2a492367bc1f4fdbf9fe81d26b77488a0125858dcf61a21e33e86db0279840d46fa2cdeb011956b62691c8bb51ca8340c8a562f0f89e764d51f4df6948f2c7',
      }
    }
    )
      .then(res => {
        this.setState({
          partner: res.data.partner,
          service: res.data.service,
          step_step_job: res.data.step_step_job,
          last_posistion: res.data.last_posistion,
          curr_service: res.data.curr_service,
          customer: res.data.customer,
          loading:true,
        });
    })


    




  }

  render() {
    const directionsService = new google.maps.DirectionsService();

    const origin = { lat: parseFloat(this.state.last_posistion.lat), lng: parseFloat(this.state.last_posistion.long) };
    const destination = { lat: parseFloat(this.state.customer.latitude), lng: parseFloat(this.state.customer.longitude) };

    directionsService.route(
      {
        origin: origin,
        destination: destination,
        travelMode: google.maps.TravelMode.DRIVING,
        avoidHighways : true,
      },
      (result, status) => {
        if (status === google.maps.DirectionsStatus.OK) {
          this.setState({
            directions: result
          });
        } else {
          console.error(`error fetching directions ${result}`);
        }
      }
    );
    
    const GoogleMapExample = withGoogleMap(props => (
      <GoogleMap
        defaultCenter={origin}
        defaultZoom={13}
      >
        <DirectionsRenderer
          directions={this.state.directions}
        />
      </GoogleMap>
    ));

    

    return (
      <div>
        
      
        <div className="grid 2xl:grid-cols-7 xl:grid-cols-7 lg:grid-cols-7 md:grid-cols-7">
          <div className="2xl:col-span-2 xl:col-span-2 lg:col-span-2 md:col-span-2  p-4">

            <div className="card shadow border-1 rounded-lg">
              <div className="card-body p-3 text-center">
                <h2 className="card-title mb-0">Your Service Tracking</h2> 
              </div>
            </div> 

            <div className="card shadow mt-3 rounded-lg overscroll-auto">
              <div className="card-body p-3">
                  <div className="flex items-center">
                     <div className="flex avatar ">
                      <div className="rounded-full w-14 h-14">  
                                          
                        <img src={this.state.partner.photo || 'https://olla.ws/images/partners/default.jpg' } ></img>
                      </div>
                    </div> 
                    <div className=" pl-3 columns-1">
                      
                      
                      <h2 className="font-bold">
                          { this.state.partner.partner_name || <Skeleton height={15}width={200}viewBox="0 0 400 200"backgroundColor="#d9d9d9"foregroundColor="#ecebeb"></Skeleton>}</h2>
                      <p>{this.state.partner.partner_code || <Skeleton height={15}width={200}viewBox="0 0 400 200"backgroundColor="#d9d9d9"foregroundColor="#ecebeb"></Skeleton>}</p>
                    </div>
                        
                  </div>

                  <hr className="mt-2 mb-2 border-1"></hr>

                  <div className="flex items-center ">
                    
                    <div className="pl-3 column-1">
                      <h2 className="font-bold">{this.state.partner.plat || <Skeleton height={15}width={250}viewBox="0 0 400 200"backgroundColor="#d9d9d9"foregroundColor="#ecebeb"></Skeleton>}  {this.state.partner.merk}</h2>
                      <p className="text-sm">{this.state.partner.mobile_phone || <Skeleton height={15}width={270}viewBox="0 0 400 200"backgroundColor="#d9d9d9"foregroundColor="#ecebeb"></Skeleton>}</p>
                    </div>
                        
                  </div>

                  <hr className="mt-2 mb-2 border-1"></hr>
                  
                  <div className="flex items-center ">
                    
                    <div className="pl-3 column-1">
                      <p className="text-sm">No. Order</p>
                      <h2 className="font-bold">
                      {this.state.partner.order_id || <Skeleton height={15}width={270}viewBox="0 0 400 200"backgroundColor="#d9d9d9"foregroundColor="#ecebeb" count={2}></Skeleton>}
                      </h2>
                      <ul class="list-disc pl-6"> 
                        {this.state.service.map(item =>

                        <li>
                          <div className="dropdown">
                            <div tabindex="0" className="m-1 text-sm"><a  href="#">{item.service_name} {item.package}</a></div> 
                            <ul tabindex="0" className="p-2 shadow menu dropdown-content bg-base-100 rounded-box w-80 steps steps-vertical">
                              <li className="step step-primary">Ambil Order</li> 
                              <li className="step step-primary">Sampai Rumah Pelanggan</li> 
                              <li className="step step-primary">
                                <div className="grid grid-cols-3 items-center justify-items-center">
                                  <div className="column-1 col-span-2">
                                    Sebelum Melakukan 
                                  </div>
                                  <div className="column-1">
                                    <label for="my-modal-2" class=" modal-button">
                                      <img src="https://picsum.photos/id/1005/200/200" class="mask mask-squircle h-14 w-14  shadow-xl" ></img>
                                    </label> 
                                  </div>
                                </div>
                              </li> 
                              <li class="step">
                                <div className="grid grid-cols-3 items-center justify-items-center">
                                  <div className="column-1 col-span-2">
                                    Sesudah Melakukan 
                                  </div>
                                  <div className="column-1">
                                    <img src="" class="mask mask-squircle h-14 w-14  shadow-xl" ></img>
                                  </div>
                                </div>
                              </li>
                              <li class="step">Selesai Servis</li>
                            </ul>
                          </div>
                        </li>

                         )}
                        
                      </ul>
                    </div>
                        
                  </div>

                  <hr className="mt-2 mb-2 border-1"></hr>
                  
                  <div className=" items-center ">
                    <div className="columns-1">
                      <h2 className="font-bold pl-6">{this.state.curr_service.service || <Skeleton height={15}viewBox="0 0 600 500"backgroundColor="#d9d9d9"foregroundColor="#ecebeb" count={10}></Skeleton>}</h2>
                    </div>
                    <div className="pl-3 columns-1">
                      <ul class="steps steps-vertical">
                      {this.state.step_step_job.map(item =>
                        <li class={item.is_done == 1 ? 'step step-primary text-sm' : 'step'}>
                          <div className="grid grid-cols-3 items-center justify-items-center">
                            <div className="column-1 col-span-2 2xl:text-xs xl:text-xs lg:text-xs">
                              {item.name} 
                            </div>
                            <div className="column-1">
                              <label for={'my-modal-'+item.name} class=" modal-button">
                                {item.name != 'Mengambil Foto (sebelum melakukan)' && item.name != 'Mengambil Foto (sesudah melakukan)' ? '' :<img src={item.images} className="mask mask-squircle h-14 w-14  shadow-xl" ></img>}
                              </label> 
                            </div>
                          </div>
                          
                        
                        <input type="checkbox" id={'my-modal-'+item.name} class="modal-toggle"></input>
                        <div class="modal " >
                          <div class="modal-box 2xl:w-60 xl:w-60 lg:w-60 md:w-60 sm:w-full xs:w-full">
                            <img src={item.images} class="mask mask-squircle h-34 w-34  shadow-xl" ></img>
                            <div class="modal-action ">
                              <label for={'my-modal-'+item.name} class="btn w-full">Close</label>
                            </div>
                          </div>
                        </div>
                        </li>
                      )}
                        
                      </ul>
                    </div>
                        
                  </div>

              </div>
            </div> 

          </div>
          <div className="2xl:col-span-5 xl:col-span-5 lg:col-span-5 md:col-span-5">
            <GoogleMapExample
              containerElement={<div className="2xl:h-screen xl:h-full lg:h-full md:h-full sm:h-96 h-96" />}
              mapElement={<div className="2xl:h-screen xl:h-full lg:h-full md:h-full sm:h-96 h-96"  />}
            />
          </div>



          
          



        </div>


        


        
        
      </div>
    );
  }
}

export default Map;
