import React from 'react';
import { render } from 'react-dom';
import { withScriptjs } from 'react-google-maps';
import Map from './Map';
import './index.css';

const App = () => {
  const MapLoader = withScriptjs(Map);

  return (
    <MapLoader
      googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyDInMvHHrMyFzMpLXW3LPpd4aMuDOkqoaE"
      loadingElement={<div style={{ height: '100%' }} />}
    />
  );
};

render(<App />, document.getElementById('root'));
